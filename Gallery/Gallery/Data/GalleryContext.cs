﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Gallery.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Gallery.Models
{
    public class GalleryContext : IdentityDbContext
    {
        public GalleryContext (DbContextOptions<GalleryContext> options)
            : base(options)
        {
        }

        public DbSet<Image> Image { get; set; }

        public DbSet<Album> Album { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //builder.Seed();
        }
    }
}
