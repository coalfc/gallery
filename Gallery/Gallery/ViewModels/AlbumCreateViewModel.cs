﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gallery.ViewModels
{
    public class AlbumCreateViewModel
    {
        public int Id { get; set; }
        [Required, MaxLength(40)]
        public string Name { get; set; }
        [Required]
        public List<IFormFile> Images { get; set; }
        [Required, MaxLength(40)]
        public string ImageName { get; set; }
        [Required, MaxLength(40)]
        public string ImageLocation { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime ImageDateTime { get; set; }
    }
}
