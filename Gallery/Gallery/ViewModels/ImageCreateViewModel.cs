﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Gallery.ViewModels
{
    public class ImageCreateViewModel
    {
        public int Id { get; set; }
        [Required,MaxLength(40)]
        public string Name { get; set; }
        [Required, MaxLength(40)]
        public string Location { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateTime { get; set; }
        [Required]
        public IFormFile Photo { get; set; }
        public string AlbumName { get; set; }
    }
}
