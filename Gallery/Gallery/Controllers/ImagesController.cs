﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Gallery.Models;
using Gallery.ViewModels;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Gallery.Controllers
{
    public class ImagesController : Controller
    {
        private readonly GalleryContext _context;

        public ImagesController(GalleryContext context)
        {
            _context = context;
        }

        // GET: Images
        /*public async Task<IActionResult> Index()
        {
            return View(await _context.Image.ToListAsync());
        }*/

        public async Task<IActionResult> Index(int? id, string NameSearch = null, string LocationSearch = null, string DaySearch = null,
        string MonthSearch = null, string YearSearch = null)
        {
            //var img = from i in _context.Image join a in _context.Album on i.AlbumName equals a.Name where a.Id == id select i; 
            var img = _context.Image.Where(x => x.AlbumId == id);

            if (!String.IsNullOrEmpty(NameSearch))
            {
                img = img.Where(s => s.Name.Contains(NameSearch));
            }
            else if (!String.IsNullOrEmpty(LocationSearch))
            {
                img = img.Where(s => s.Location.Contains(LocationSearch));
            }
            else if (!String.IsNullOrEmpty(DaySearch))
            {
                img = img.Where(s => s.DateTime.Day.ToString().Equals(DaySearch));
            }
            else if (!String.IsNullOrEmpty(MonthSearch))
            {
                img = img.Where(s => s.DateTime.Month.ToString().Equals(MonthSearch));
            }
            else if (!String.IsNullOrEmpty(YearSearch))
            {
                img = img.Where(s => s.DateTime.Year.ToString().Equals(YearSearch));
            }

            return View(await img.ToListAsync());
        }

        // GET: Images/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var image = await _context.Image
                .FirstOrDefaultAsync(m => m.Id == id);
            if (image == null)
            {
                return NotFound();
            }

            return View(image);
        }

        // GET: Images/Create
        public IActionResult Create()
        {
            ViewBag.ListOfAlbums = _context.Album.Select(x => x.Name);
            return View();
        }

        // POST: Images/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ImageCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var albumId = _context.Album.Where(s => s.Name == model.AlbumName).Select(x => x.Id).First();
                var image = new Image
                {
                    Name = model.Name,
                    Location = model.Location,
                    DateTime = model.DateTime,
                    AlbumId = albumId
                };
                using (var memoryStream = new MemoryStream())
                {
                    await model.Photo.CopyToAsync(memoryStream);
                    image.bytePhoto = memoryStream.ToArray();
                }
                _context.Add(image);
                await _context.SaveChangesAsync();
                return RedirectToAction("details", new { id = image.Id }); //"details", new { id = newEmployee.Id }
            }
            return View();
        }

        // GET: Images/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewData["Albums"] = from x in _context.Album select x;
            var image = await _context.Image.FindAsync(id);
            if (image == null)
            {
                return NotFound();
            }
            return View(image);
        }

        // POST: Images/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Image image)
        {
            if (id != image.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(image);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImageExists(image.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("details", new { id = image.Id });
            }
            return View(image);
        }

        // GET: Images/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var image = await _context.Image
                .FirstOrDefaultAsync(m => m.Id == id);
            if (image == null)
            {
                return NotFound();
            }

            return View(image);
        }

        // POST: Images/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var image = await _context.Image.FindAsync(id);
            _context.Image.Remove(image);
            await _context.SaveChangesAsync();
            return RedirectToAction("index", new { id = image.AlbumId });
        }

        private bool ImageExists(int id)
        {
            return _context.Image.Any(e => e.Id == id);
        }
    }
}
