﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Gallery.Migrations
{
    public partial class AddAlbums : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Image_Album_AlbumId",
                table: "Image");

            migrationBuilder.DropColumn(
                name: "AlbumName",
                table: "Image");

            migrationBuilder.AlterColumn<int>(
                name: "AlbumId",
                table: "Image",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Album_AlbumId",
                table: "Image",
                column: "AlbumId",
                principalTable: "Album",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Image_Album_AlbumId",
                table: "Image");

            migrationBuilder.AlterColumn<int>(
                name: "AlbumId",
                table: "Image",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "AlbumName",
                table: "Image",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Album_AlbumId",
                table: "Image",
                column: "AlbumId",
                principalTable: "Album",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
